package com.app.materiallists;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.materiallists.holders.SingleLineAvatarHolder;

public abstract class SingleLinedAvatarAdapter extends RecyclerView.Adapter<SingleLineAvatarHolder> {

    @NonNull
    protected final LayoutInflater inflater;

    public SingleLinedAvatarAdapter(@NonNull Context context){
        inflater = LayoutInflater.from(context);
    }

    @Override
    public SingleLineAvatarHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return SingleLineAvatarHolder.create(inflater, parent);
    }

}