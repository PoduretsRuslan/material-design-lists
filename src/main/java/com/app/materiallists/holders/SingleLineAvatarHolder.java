package com.app.materiallists.holders;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.materiallists.R;

public class SingleLineAvatarHolder extends ClickHolder {


    public final TextView mainTextView;
    public final ImageView leftIcon;

    private SingleLineAvatarHolder(View itemView) {
        super(itemView);
        leftIcon = itemView.findViewById(R.id.leftIcon);
        mainTextView = itemView.findViewById(R.id.mainTextView);
    }

    public static SingleLineAvatarHolder create(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent) {
        return new SingleLineAvatarHolder(inflater.inflate(R.layout.single_line_avatar_item, parent, false));
    }

}
