package com.app.materiallists.holders;

public interface PositionClickListener {

    void onClick(int position);
}
