package com.app.materiallists.holders;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.materiallists.R;

public class TwoLineAvatarHolder extends ClickHolder {

    public final TextView mainTextView;
    public final TextView secondaryTextView;
    public final ImageView leftIcon;

    private TwoLineAvatarHolder(View itemView) {
        super(itemView);
        leftIcon = itemView.findViewById(R.id.leftIcon);
        mainTextView = itemView.findViewById(R.id.mainTextView);
        secondaryTextView = itemView.findViewById(R.id.secondaryTextView);
    }

    public static TwoLineAvatarHolder create(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent) {
        return new TwoLineAvatarHolder(inflater.inflate(R.layout.two_line_avatar_item, parent, false));
    }

}
