package com.app.materiallists.holders;


import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ClickHolder extends RecyclerView.ViewHolder {

    public static final long DEFAULT_CLICK_DELAY = 500;

    protected PositionClickListener itemClickListener;
    protected long lastTimeClick = 0;

    public ClickHolder(View itemView) {
        super(itemView);
        initClickListener(itemView);
    }

    protected void initClickListener(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(itemClickListener!=null){
                    long time = System.currentTimeMillis();
                    if (time < lastTimeClick + DEFAULT_CLICK_DELAY) {
                        return;
                    }
                    lastTimeClick = time;
                    int position = getAdapterPosition();
                    if (position >= 0) {
                        itemClickListener.onClick(position);
                    }
                }
            }
        });
    }

    public void setItemClickListener(PositionClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void clearClicks() {
        itemClickListener = null;
    }

}
