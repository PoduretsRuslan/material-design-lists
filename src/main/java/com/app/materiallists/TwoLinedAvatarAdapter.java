package com.app.materiallists;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.materiallists.holders.TwoLineAvatarHolder;

public abstract class TwoLinedAvatarAdapter extends RecyclerView.Adapter<TwoLineAvatarHolder> {

    @NonNull
    protected final LayoutInflater inflater;

    public TwoLinedAvatarAdapter(@NonNull Context context){
        inflater = LayoutInflater.from(context);
    }

    @Override
    public TwoLineAvatarHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return TwoLineAvatarHolder.create(inflater, parent);
    }

}
